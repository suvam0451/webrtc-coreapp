import React from "react";
import { v1 as uuid } from "uuid";

interface IProps {
  history: any;
}
/** User A can create room */
const CreateRoom = (props: IProps) => {
  function create() {
    const id = uuid();
    props.history.push(`/room/${id}`);
  }

  return <button>Create Room</button>;
};

export default CreateRoom;

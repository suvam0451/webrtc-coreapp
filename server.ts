import express from "express";
import http from "http";
import socket from "socket.io";

const app = express();

const server = http.createServer(app);

const io = socket(server);

const rooms = {};

// On server connection
io.on("connection", (socket) => {
  // gets fired on client side
  socket.on("join room", (roomID) => {
    if (rooms[roomID]) {
      rooms[roomID].push(socket.id);
    } else {
      rooms[roomID] = [socket.id];
    }

    // Somebody else is with me
    const otherUser = rooms[roomID].find((id) => id != socket.id);

    if (otherUser) {
      // User B gets notified that he joined a room
      socket.emit("other user", otherUser);
      // User A gets notified of user B
      socket.io(otherUser).emit("user joined", socket.id);
    }
  });

  // Payload will have my ID + offer I am trying to send
  socket.on("offer", (payload) => {
    io.to(payload.target).emit("offer", payload);
  });

  socket.on("answer", (payload) => {
    io.to(payload.target).emit("answer", payload);
  });
});

server.listen(8000, () => console.log("server is running on port 8000"));
